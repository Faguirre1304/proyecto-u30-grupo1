const mongodb = require("mongoose");

mongodb.connect(
  "mongodb://localhost:27017/db_OmegaPetShop_U30-1",

  {
    useNewUrlParser: true,
    //useCreateIndex: true,
    useUnifiedTopology: true,
    //useFindAndModify: false,
  },
  //promise es una funcion que queda a la escucha que se activa cua
  (err, res) => {
    if (err) {
      throw err;
    } else {
      console.log("conexion Establecida con la base de datos");
    }
  }
);

module.exports = mongodb;
