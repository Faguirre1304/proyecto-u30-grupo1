// CONFIGURACION GLOBAL DEL PROYECTO
//CARGA DE DEPENDENCIAS
var express = require("express");
var app = express();


//importamos el control de CORS
const cors = require('cors');
app.use(cors());

var bodyParser = require("body-parser");
//var methodOverride = require('method-override');
var mongoose = require("mongoose");

//USO DE DEPENDENCIAS

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//CONFIGURACION DE LA CABECERA Y CORS(CONTROL PARA LAS PETICIONES HTTP ReQUEST)
app.use((req, res, next) => {
  res.header("access-control-allow-origin", "*");

  res.header(
    "access-control-allow-headers",
    "authorization, X-API-KEY, origin, X-Requested-With, content-type, accept, Access-Control-Allow-Request-Method"
  );

  res.header(
    "Access-Control-Allow-Request-Method",
    "GET, POST, PUT, DELETE,OPTIONS"
  );

  res.header("Allow", "GET, POST, PUT, DELETE,OPTIONS");

  next();
});

//RUTAS
app.use(require('./routers/routers'));



module.exports = app;
