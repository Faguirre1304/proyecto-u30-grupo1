const { Router } = require("express");
const router = Router();

//ENPOINT ALIMENTOS
var controllerProductoComida = require("..//src/controllers/controllerProductoComida");
router.post("/crearunidadalimento", controllerProductoComida.saveAlimento);
router.get("/buscarunidadalimento/:id",controllerProductoComida.buscarAlimentoId);
router.get("/listaralimentos/:id?", controllerProductoComida.listarTodo);
router.put("/actualizaralimento/:id", controllerProductoComida.updateProducto);
router.delete("/eliminaralimento/:id", controllerProductoComida.deleteProducto);

//ENPOINT VESTUARIO

var controllerProductoVestuario = require("..//src/controllers/controllerProductoVestuario");
router.post("/crearunidadvestuario",controllerProductoVestuario.saveVestuario);
router.get("/buscarunidadvestuario/:id",controllerProductoVestuario.buscarVestuarioId);
router.get("/listarvestuario/:id?",controllerProductoVestuario.listarTodoVestuario);
router.put("/actualizarvestuario/:id",controllerProductoVestuario.updateVestuario);
router.delete("/eliminarvestuario/:id",controllerProductoVestuario.deleteVestuario);

//ENPOINT JUGUETES
var controllerProductoJuguetes = require("../src/controllers/controllerProductoJuguetes");

router.post("/crearunidadjuguete", controllerProductoJuguetes.saveJuguete);
router.get(
  "/buscarunidadjuguete/:id",
  controllerProductoJuguetes.buscarJugueteId
);
router.get(
  "/listarjuguetes/:id?",
  controllerProductoJuguetes.listarTodoJuguetes
);
router.put("/actualizarjuguete/:id", controllerProductoJuguetes.updateJuguete);
router.delete("/eliminarjuguete/:id", controllerProductoJuguetes.deleteJuguete);

//ENPOINT CLIENTES

var controllerCliente = require("../src/controllers/controllerCliente");

router.post("/crearcliente", controllerCliente.guardarCliente);
router.get("/buscarcliente/:id", controllerCliente.buscarClienteId);
router.get("/listarclientes/:id?", controllerCliente.listarClientes);
router.put("/actualizarcliente/:id", controllerCliente.updateCliente);
router.delete("/eliminarcliente/:id", controllerCliente.deleteCliente);

//ENPOINT ADMINISTRADOR

var controllerAdmon = require("../src/controllers/controllerAdmon");

router.post("/crearadmon", controllerAdmon.guardarAdmon);
router.get("/buscaradmon/:id", controllerAdmon.buscarAdmonId);
router.get("/listaradmon/:id?", controllerAdmon.listarAdmon);
router.put("/actualizaradmon/:id", controllerAdmon.updateAdmon);
router.delete("/eliminaradmon/:id", controllerAdmon.deleteAdmon);

module.exports = router;
