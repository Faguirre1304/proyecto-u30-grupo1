var mongodb = require("mongoose");

var schema = mongodb.Schema;

var productoSchema = schema({
  nombre_producto: String,
  categoria_producto: String,
  imagen: String,
  precio: Number,
  stock: Number,
  codigo: Number,
  tipo_animal: String,
  descripcion: String,
});

const productoJuguetes = mongodb.model("tbc_productos_Juguete", productoSchema);
module.exports = productoJuguetes;
