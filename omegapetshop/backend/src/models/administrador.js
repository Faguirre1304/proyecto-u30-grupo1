var mongodb = require("mongoose");

var schema = mongodb.Schema;

var admonSchema = schema({
  nombre: String,
  correo: String,
  documento: String,
  password: String,
});

const Admon = mongodb.model('tbc_administrador',admonSchema)

module.exports = Admon;


