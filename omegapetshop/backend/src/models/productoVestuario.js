var mongodb = require("mongoose");

var schema = mongodb.Schema;

var productoSchema = schema({
  nombre_producto: String,
  categoria_producto: String,
  imagen: String,
  precio: Number,
  stock: Number,
  codigo: Number,
  tallas: Object,
  tipo_animal: String,
  descripcion: String,
})

const producto_Vestuario = mongodb.model('tbc_productos_vestuario',productoSchema);
module.exports = producto_Vestuario;