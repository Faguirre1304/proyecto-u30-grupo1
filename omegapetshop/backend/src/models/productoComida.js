var mongodb = require("mongoose");

var schema = mongodb.Schema;

var productoSchema = schema({
  nombre_producto: String,
  categoria_producto: String,
  imagen: String,
  precio: Number,
  stock: Number,
  codigo: Number,
  gramaje: Object,
  tipo_animal: String,
  descripcion: String,
});

const producto_Alimento = mongodb.model(
  "tbc_productos_alimento",
  productoSchema
);
module.exports = producto_Alimento;
