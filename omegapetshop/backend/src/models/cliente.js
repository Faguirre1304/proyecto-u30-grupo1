var mongodb = require("mongoose");

var schema = mongodb.Schema;

var clienteSchema = schema({
  nombre_cliente: String,
  correo: String,
  password: String,
  direccion: String,
});


const cliente = mongodb.model('tbc_cliente',clienteSchema);

module.exports = cliente;


