const Admon = require("../models/administrador")

//metodos CRUD

//metodo grardar -- POST

function guardarAdmon(req, res) {
     var admon = new Admon(req.body);
   
     admon.save((err, result) => {
       res.status(200).send({ message: result });
     });
   }


//metodo buscar por id -- GET

function buscarAdmonId(req, res) {
     var idAdmon = req.params.id;
   
     Admon.findById(idAdmon).exec((err, result) => {
       if (err) {
         res
           //error 500 es porque hay algo mal escrito en el codigo
           .status(500)
           .send({ message: "Error al momento de procesar en el backend" });
       } else {
         if (!result) {
           //error 404 no encuentra el valor en el backend // error de usuario
           res.status(404).send({
             message: "Producto no encontrado, verificar entrada",
           });
         } else {
           //conexion exitosa
           res.status(200).send({ message: result });
         }
       }
     });
   }

//metodo buscar por id y listar todo -- GET

function listarAdmon(req, res) {
     var idAdmon = req.params.id;
     if (!idAdmon) {
       //sort es una funcion para ordenar los datos
       var result = Admon.find({}).sort("nombre");
     } else {
       var result = Admon.find({ _id: idAdmon }).sort("nombre");
     }
     result.exec(function (err, result) {
       if (err) {
         res
           //error 500 es porque hay algo mal escrito en el codigo
           .status(500)
           .send({ message: "Error al momento de procesar en el backend" });
       } else {
         if (!result) {
           //error 404 no encuentra el valor en el backend // error de usuario
           res.status(404).send({
             message: "Se ha ingresado valores incorrectos y no se puede procesar",
           });
         } else {
           //conexion exitosa
           res.status(200).send({ message: result });
         }
       }
     });
   }
   

   //metodo actualizar por id -- PUT

function updateAdmon(req, res) {
     var idAdmon = req.params.id;
   
     Admon.findOneAndUpdate(
       { _id: idAdmon },
       req.body,
       { new: true },
       function (err, Admon) {
         if (err) res.send(err);
         res.json(Admon);
       }
     );
   }

//metodo eliminar por id -- DELETE

function deleteAdmon(req, res) {
     var idAdmon = req.params.id;
   
     Admon.findByIdAndDelete(idAdmon, function (err, Admon) {
       if (err) {
         return res.send(500, {
           message: "No se ha encontrado la ruta a eliminar",
         });
       }
       return res.json(Admon);
     });
   }
   
   module.exports ={
     guardarAdmon,
     buscarAdmonId,
     listarAdmon,
     updateAdmon,
     deleteAdmon
   };


