const vestuario = require("..//models/productoVestuario");

// function prueba(req, res) {
//   res.status(200).send({ message: "la conexion fue satisfactoria" });
// }

//METODOS CRUD
function saveVestuario(req, res) {
  var vestuarios = new vestuario(req.body);
  //FUNCION DE TIPO PROMESA
  vestuarios.save((err, result) => {
    res.status(200).send({ message: result });
  });
}

function buscarVestuarioId(req, res) {
  var idVestuario = req.params.id;
 vestuario.findById(idVestuario).exec((err, result) => {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Producto no encontrado, verificar entrada",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

function listarTodoVestuario(req, res) {
  var idVestuario = req.params.id;
  if (!idVestuario) {
    //sort es una funcion para ordenar los datos
    var result = vestuario.find({}).sort("nombre_producto");
  } else {
    var result = vestuario.find({ _id: idVestuario }).sort("nombre_producto");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

function updateVestuario(req, res) {
  var idVestuario = req.params.id;

  vestuario.findOneAndUpdate(
    { _id: idVestuario },
    req.body,
    { new: true },
    function (err, vestuario) {
      if (err) res.send(err);
      res.json (vestuario);
    }
  );
}

function deleteVestuario(req, res) {
  var idVestuario = req.params.id;

 vestuario.findByIdAndDelete(idVestuario, function (err, vestuario) {
    if (err) {
      return res.send(500, {
        message: "No se ha encontrado la ruta a eliminar",
      });
    }
    return res.json (vestuario);
  });
}



module.exports = {
  saveVestuario,
  buscarVestuarioId,
  listarTodoVestuario,
  updateVestuario,
  deleteVestuario
};
