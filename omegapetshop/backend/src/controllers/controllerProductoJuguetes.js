const juguetes = require("..//models/productoJuguetes");

// function prueba(req, res) {
//   res.status(200).send({ message: "la conexion fue satisfactoria" });
// }

//METODOS CRUD
//funcion guardar - post
function saveJuguete(req, res) {
  var juguete = new juguetes(req.body);
  //FUNCION DE TIPO PROMESA
  juguete.save((err, result) => {
    res.status(200).send({ message: result });
  });
}

//METODOS CRUD
//funcion buscar - get
function buscarJugueteId(req, res) {
  var idJuguetes = req.params.id;
  juguetes.findById(idJuguetes).exec((err, result) => {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Producto no encontrado, verificar entrada",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

//METODOS CRUD
//funcion listar - get

function listarTodoJuguetes(req, res) {
  var idJuguetes = req.params.id;
  if (!idJuguetes) {
    //sort es una funcion para ordenar los datos
    var result = juguetes.find({}).sort("nombre_producto");
  } else {
    var result = juguetes.find({ _id: idJuguetes }).sort("nombre_producto");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

//METODOS CRUD
//funcion actualizar - put

function updateJuguete(req, res) {
  var idJuguetes = req.params.id;

  juguetes.findOneAndUpdate(
    { _id: idJuguetes },
    req.body,
    { new: true },
    function (err, juguetes) {
      if (err) res.send(err);
      res.json(juguetes);
    }
  );
}

//METODOS CRUD
//funcion eliminar - delete

function deleteJuguete(req, res) {
  var idJuguetes = req.params.id;

  juguetes.findByIdAndDelete(idJuguetes, function (err, juguetes) {
    if (err) {
      return res.send(500, {
        message: "No se ha encontrado la ruta a eliminar",
      });
    }
    return res.json(juguetes);
  });
}

module.exports = {
  saveJuguete,
  buscarJugueteId,
  listarTodoJuguetes,
  updateJuguete,
  deleteJuguete,
};
