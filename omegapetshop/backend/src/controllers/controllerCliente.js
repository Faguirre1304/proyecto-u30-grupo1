const clienteOPS = require("../models/cliente");

//metodos CRUD

//metodo grardar -- POST

function guardarCliente(req, res) {
  var cliente = new clienteOPS(req.body);

  cliente.save((err, result) => {
    res.status(200).send({ message: result });
  });
}

//metodo buscar por id -- GET

function buscarClienteId(req, res) {
  var idCliente = req.params.id;

  clienteOPS.findById(idCliente).exec((err, result) => {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Producto no encontrado, verificar entrada",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

//metodo buscar por id y listar todo -- GET

function listarClientes(req, res) {
  var idCliente = req.params.id;
  if (!idCliente) {
    //sort es una funcion para ordenar los datos
    var result = clienteOPS.find({}).sort("nombre_cliente");
  } else {
    var result = clienteOPS.find({ _id: idCliente }).sort("nombre_cliente");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

//metodo actualizar por id -- PUT

function updateCliente(req, res) {
  var idCliente = req.params.id;

  clienteOPS.findOneAndUpdate(
    { _id: idCliente },
    req.body,
    { new: true },
    function (err, clienteOPS) {
      if (err) res.send(err);
      res.json(clienteOPS);
    }
  );
}

//metodo eliminar por id -- DELETE

function deleteCliente(req, res) {
  var idCliente = req.params.id;

  clienteOPS.findByIdAndDelete(idCliente, function (err, clienteOPS) {
    if (err) {
      return res.send(500, {
        message: "No se ha encontrado la ruta a eliminar",
      });
    }
    return res.json(clienteOPS);
  });
}

module.exports ={
  guardarCliente,
  buscarClienteId,
  listarClientes,
  updateCliente,
  deleteCliente
};
