const concentrado = require("../models/productoComida");

function prueba(req, res) {
  res.status(200).send({ message: "la conexion fue satisfactoria" });
}

//METODOS CRUD
function saveAlimento(req, res) {
  var alimento = new concentrado(req.body);
  //FUNCION DE TIPO PROMESA
  alimento.save((err, result) => {
    res.status(200).send({ message: result });
  });
}

function buscarAlimentoId(req, res) {
  var idAlimento = req.params.id;
  concentrado.findById(idAlimento).exec((err, result) => {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Producto no encontrado, verificar entrada",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

function listarTodo(req, res) {
  var idAlimento = req.params.id;
  if (!idAlimento) {
    //sort es una funcion para ordenar los datos
    var result = concentrado.find({}).sort("nombre_producto");
  } else {
    var result = concentrado.find({ _id: idAlimento }).sort("nombre_producto");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        //error 500 es porque hay algo mal escrito en el codigo
        .status(500)
        .send({ message: "Error al momento de procesar en el backend" });
    } else {
      if (!result) {
        //error 404 no encuentra el valor en el backend // error de usuario
        res.status(404).send({
          message: "Se ha ingresado valores incorrectos y no se puede procesar",
        });
      } else {
        //conexion exitosa
        res.status(200).send({ message: result });
      }
    }
  });
}

function updateProducto(req, res) {
  var idAlimento = req.params.id;

  concentrado.findOneAndUpdate(
    { _id: idAlimento },
    req.body,
    { new: true },
    function (err, concentrado) {
      if (err) res.send(err);
      res.json(concentrado);
    }
  );
}

function deleteProducto(req, res) {
  var idAlimento = req.params.id;

  concentrado.findByIdAndDelete(idAlimento, function (err, concentrado) {
    if (err) {
      return res.send(500, {
        message: "No se ha encontrado la ruta a eliminar",
      });
    }
    return res.json(concentrado);
  });
}

module.exports = {
  prueba,
  saveAlimento,
  buscarAlimentoId,
  listarTodo,
  updateProducto,
  deleteProducto,
};
