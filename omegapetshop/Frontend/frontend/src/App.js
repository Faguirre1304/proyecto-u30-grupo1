import React from "react";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// import "./App.css";

import CompProductoAlmientoListar from './components/ProductoAlimentoList'
import CompProductoAlmientoCrear from './components/ProductoAlimentoCrear';
import CompProductoAlmientoEdit from './components/ProductoAlimentoEdit';
import CompProductoVestuarioListar from "./components/ProductoVestuarioList";
import CompProductoVestuarioEdit from "./components/ProductoVestuarioEdit";
import CompProductoVestuarioCrear from "./components/ProductoVestuarioCrear";
import CompAdminListar from "./components/AdministradorList";
import CompAdminEdit from "./components/AdministradorEdit";
import CompAdminCrear from "./components/AdministradorCrear";
import Navigator from './components/Navigator'




function App() {
  
  return (

    
    <Router>
      <Routes>
          <Route path="/" element={<Navigator/>}/>
          <Route path="/alimento" element={<CompProductoAlmientoListar/>}/>
          <Route path="/crearalimento" element={<CompProductoAlmientoCrear/>}/> 
          <Route path="/editaralimento/:id" element={<CompProductoAlmientoEdit/>}/>
          <Route path="/vestuario" element={<CompProductoVestuarioListar/>}/>
          <Route path="/editarvestuario/:id" element={<CompProductoVestuarioEdit/>}/>
          <Route path="/crearvestuario" element={<CompProductoVestuarioCrear/>}/>
          <Route path="/admin" element={<CompAdminListar/>}/>
          <Route path="/editaradmin/:id" element={<CompAdminEdit/>}/>
          <Route path="/crearadmin" element={<CompAdminCrear/>}/>
          
      </Routes>
    </Router>

    
  );
}



export default App;
