import axios from "axios";

//HOOK --> GANCHOS --> Gestionar los estados del componente
import { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import "./Estilos.css";

const URI = "http://localhost:4200/";

const CompProductoAlmientoListar = () => {
  const [productoAlmento, setAlimento] = useState([]);
  useEffect(() => {
    getAlimentos();
  }, []);

  //METODOS DEL COMPONENTE

  //METODO PARA LISTAR LAS RUTAS DE VUELO

  const getAlimentos = async () => {
    const res = await axios.get(`${URI}listaralimentos/`);

    setAlimento(res.data.message);
  };


  //METODO PARA ELIMINAR LAS RUTAS DE VUELO

  const deleteProductoAlimento = async (id) => {
     await axios.delete(`${URI}eliminaralimento/${id}`);
     getAlimentos();
   };

   return(
     <div className="container-tabla">
       <img src="../images/LogoPaginaNT.png" alt="logo" />
      <h1 className="titulos">Lista Alimentos</h1>
      <div className="row" style={{margin:"10px"}}>
        <div className="col">
          <Link to={"/crearalimento"} className="crear" style={{marginLeft:'70px'}}>
            <i className="fa-solid fa-file-circle-plus" title="Crear">
              {" "}
            </i>{" "}
            Crear Producto
          </Link>

          <Link to={"/"} className="crear" >
            <i className="fa-solid fa-file-circle-plus" title="Home">
              {" "}
            </i>{" "}
            Menu Principal
          </Link>


          <table >
            <thead>
              <tr>
                <th>Nombre Producto</th>
                <th>Categoria Producto</th>
                <th>Precio</th>
                <th>Stock</th>
                <th>Codigo</th>
                <th>Gramaje</th>
                <th>Tipo Animal</th>
                <th>Descripcion</th>
                <th>Acciones</th>
              </tr>
            </thead>

            <tbody>
              {productoAlmento.map((alimento) => (
                <tr key={alimento._id}>
                  <td>{alimento.nombre_producto}</td>
                  <td>{alimento.categoria_producto}</td>
                  <td>{alimento.precio}</td>
                  <td>{alimento.stock}</td>
                  <td>{alimento.codigo}</td>
                  <td>{alimento.gramaje}</td>
                  <td>{alimento.tipo_animal}</td>
                  <td>{alimento.descripcion}</td>

                  <td>
                    <Link
                      to={`/editaralimento/${alimento._id}`}
                      className="fa-regular fa-pen-to-square"
                      style={{ color: "#000" }}
                    ></Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link
                      onClick={() => deleteProductoAlimento(alimento._id)}
                      className="fa-solid fa-trash-can"
                      style={{ color: "#EE3724" }}
                    ></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
 





};

export default CompProductoAlmientoListar;