import React from "react";
import {Link} from 'react-router-dom'



import './EstilosNavigator.css'
function home() {
  return (
    <header>
          <Link to={"/"} className="logo">
          <img src="../images/LogoPaginaNT.png" alt="logo" />
        <h2>OMEGA PETSHOP</h2>
          </Link>
 

      <nav className="nav">
        <Link to={'/alimento'} className='link'>
            <i className="fa-solid fa-bowl-food" style={{color:'white',margin:'8px', fontSize:'25px'}}></i>
          Alimentos
        </Link>

        <Link to={'/vestuario'} className='link'>
            <i className="fa-solid fa-shirt" style={{color:'white',margin:'8px', fontSize:'25px'}}></i>
          Vestuario
        </Link>

        <Link to={'/juguetes'} className='link'>
            <i class="fa-solid fa-democrat" style={{color:'white',margin:'8px', fontSize:'25px'}}></i>
          Juguetes
        </Link>

        <Link to={'/facturacion'} className='link'>
        <i class="fa-solid fa-cash-register" style={{color:'white',margin:'8px', fontSize:'25px'}}></i>
          Facturación
        </Link>

        <Link to={'/admin'} className='link'>
        <i class="fa-solid fa-mug-hot" style={{color:'white',margin:'8px', fontSize:'25px'}}></i>
          Usuarios
        </Link>
      </nav>
    </header>
  );
}

export default home;
