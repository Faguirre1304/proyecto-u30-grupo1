import axios from "axios";

//HOOK --> GANCHOS --> Gestionar los estados del componente
import { useState, useEffect } from "react";

import { Link } from "react-router-dom";

const URI = "http://localhost:4200/";

const CompAdminListar = () => {
  const [Admin, setAdmin] = useState([]);
  useEffect(() => {
    getAdmin();
  }, []);

  //METODOS DEL COMPONENTE

  //METODO PARA LISTAR USUARIOS DE TIPO ADMINISTRADOR

  const getAdmin = async () => {
    const res = await axios.get(`${URI}listaradmon/`);

    setAdmin(res.data.message);
  };

  //METODO PARA ELIMINAR USUARIOS

  const deleteAdmin = async (id) => {
    await axios.delete(`${URI}eliminaradmon/${id}`);
    getAdmin();
  };

  return (
    <div className="container-tabla">
      <img src="../images/LogoPaginaNT.png" alt="logo" />
      <h1 className="titulos">Lista Usarios</h1>
      <div className="row">
        <div className="col">
          <Link to={"/crearadmin"} className="crear">
            <i className="fa-solid fa-file-circle-plus" title="Crear">
              {" "}
            </i>{" "}
            Crear Usuario
          </Link>

          <Link to={"/"} className="crear">
            <i className="fa-solid fa-file-circle-plus" title="Home">
              {" "}
            </i>{" "}
            Menu Principal
          </Link>
          <table>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Documento</th>
                <th>Password</th>
                <th>Acciones</th>
              </tr>
            </thead>

            <tbody>
              {Admin.map((admin) => (
                <tr key={admin._id}>
                  <td>{admin.nombre}</td>
                  <td>{admin.correo}</td>
                  <td>{admin.documento}</td>
                  <td>{admin.password}</td>
                  <td>
                    <Link
                      to={`/editaradmin/${admin._id}`}
                      className="fa-regular fa-pen-to-square"
                    ></Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link
                      onClick={() => deleteAdmin(admin._id)}
                      className="fa-solid fa-trash-can"
                      style={{ color: "#F06825" }}
                    ></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default CompAdminListar;
