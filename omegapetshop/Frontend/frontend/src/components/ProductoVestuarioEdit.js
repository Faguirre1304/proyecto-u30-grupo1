import axios from "axios";

import { useEffect, useState } from "react";

import { useNavigate, useParams, Link } from "react-router-dom";

import "./Estilos.css";

const URI = "http://localhost:4200/";

const CompProductoVestuarioEdit = () => {
  //IMPLEMENTACION DE LOS HOOKS

  const [nombre_producto, setNombreProducto] = useState("");
  const [categoria_producto, setCategoriaProducto] = useState("");
  const [precio, setPrecio] = useState("");
  const [stock, setStock] = useState("");
  const [codigo, setCodigoProducto] = useState("");
  const [tallas, setTallas] = useState("");
  const [tipo_animal, setTipoAnimal] = useState("");
  const [descripcion, setDescripcion] = useState("");

  const navigate = useNavigate();
  const { id } = useParams();

  //METODO EDITAR

  const editarVestuario = async (e) => {
    e.preventDefault();
    await axios.put(`${URI}actualizarvestuario/${id}`, {
      nombre_producto: nombre_producto,
      categoria_producto: categoria_producto,
      precio: precio,
      stock: stock,
      codigo: codigo,
      tallas: tallas,
      tipo_animal: tipo_animal,
      descripcion: descripcion
    });

    //SI HAY STATUS 200
    navigate("/vestuario");
  };

  useEffect(() => {
    getVestuarioById();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getVestuarioById = async () => {
    const res = await axios.get(`${URI}buscarunidadvestuario/${id}`);
    setNombreProducto(res.data.message.nombre_producto);
    setCategoriaProducto(res.data.message.categoria_producto);
    setPrecio(res.data.message.precio);
    setStock(res.data.message.stock);
    setCodigoProducto(res.data.message.codigo);
    setTallas(res.data.message.tallas);
    setTipoAnimal(res.data.message.tipo_animal);
    setDescripcion(res.data.message.descripcion);

    
  };

  return (
    <div className="container-alimento">
      <img src="../images/LogoPaginaNT.png" alt="logo" />
      <h1>Editar Producto Vestuario</h1>
      <form onSubmit={editarVestuario}>
        

        <label className="titulos">Nombre Producto</label>
        <input
          value={nombre_producto}
          onChange={(e) => setNombreProducto(e.target.value)}
          type="text"
          className="input"
        />
        <label className="titulos">Categoria</label>
        <input
          value={categoria_producto}
          onChange={(e) => setCategoriaProducto(e.target.value)}
          type="text"
          className="input"
        />
         <label className="titulos">Precio</label>
        <input
          value={precio}
          onChange={(e) => setPrecio(e.target.value)}
          type="text"
          className="input"
        />
         <label className="titulos">Stock</label>
        <input
          value={stock}
          onChange={(e) => setStock(e.target.value)}
          type="text"
          className="input"
        />
         <label className="titulos">Codigo</label>
        <input
          value={codigo}
          onChange={(e) => setCodigoProducto(e.target.value)}
          type="text"
          className="input"
        />
         <label className="titulos">Tallas</label>
        <input
          value={tallas}
          onChange={(e) => setTallas(e.target.value)}
          type="text"
          className="input"
        />
         <label className="titulos">Tipo Animal</label>
        <input
          value={tipo_animal}
          onChange={(e) => setTipoAnimal(e.target.value)}
          type="text"
          className="input"
        />
         <label className="titulos">Descripcion</label>
        <input
          value={descripcion}
          onChange={(e) => setDescripcion(e.target.value)}
          type="text"
          className="input"
        />




        <button type="Submit" className="crear">
          Editar
        </button>
        <Link to={"/vestuario"}>
          <button type="Submit" className="crear" style={{ float: "right" }}>
            Regresar
          </button>
        </Link>

      </form>
    </div>
  );
};

export default CompProductoVestuarioEdit;