import axios from "axios";

import { useState } from "react";

import { useNavigate } from "react-router-dom";

import { Link } from "react-router-dom";

import "./Estilos.css";

const URI = "http://localhost:4200/crearunidadalimento/";

const CompProductoAlmientoCrear = () => {
  //IMPLEMENTACION DE LOS HOOKS

  const [nombre_producto, setNombreProducto] = useState("");
  const [categoria_producto, setCategoriaProducto] = useState("");
  const [precio, setPrecio] = useState("");
  const [stock, setStock] = useState("");
  const [codigo, setCodigoProducto] = useState("");
  const [gramaje, setGranaje] = useState("");
  const [tipo_animal, setTipoAnimal] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const navigate = useNavigate();

  const create = async (e) => {
    e.preventDefault();
    await axios.post(URI, {
      nombre_producto: nombre_producto.toLocaleUpperCase(),
      categoria_producto: categoria_producto.toLocaleUpperCase(),
      precio: precio.toLocaleUpperCase(),
      stock: stock.toLocaleUpperCase(),
      codigo: codigo.toLocaleUpperCase(),
      gramaje: gramaje.toLocaleUpperCase(),
      tipo_animal: tipo_animal.toLocaleUpperCase(),
      descripcion: descripcion.toLocaleUpperCase(),
    });

    //SI HAY STATUS 200
    navigate("/alimento");
  };

  return (
    <div className="container-alimento">
      <img src="../images/LogoPaginaNT.png" alt="logo" />
      <h1 className="titulos">Creación Nuevo Producto</h1>

      <form onSubmit={create}>
        <label className="titulos">Nombre Producto</label>
        <input
          value={nombre_producto}
          onChange={(e) => setNombreProducto(e.target.value)}
          type="text"
          className="input"
          placeholder="Nombre Producto"
          required
        />
        <label className="titulos">Categoria</label>
        <input
          value={categoria_producto}
          onChange={(e) => setCategoriaProducto(e.target.value)}
          type="text"
          className="input"
          placeholder="Categoria"
          required
        />
        <label className="titulos">Precio</label>
        <input
          value={precio}
          onChange={(e) => setPrecio(e.target.value)}
          type="number"
          className="input"
          placeholder="Precio"
          required
        />
        <label className="titulos">Stock</label>
        <input
          value={stock}
          onChange={(e) => setStock(e.target.value)}
          type="number"
          className="input"
          placeholder="Stock"
          required
        />
        <label className="titulos">Codigo</label>
        <input
          value={codigo}
          onChange={(e) => setCodigoProducto(e.target.value)}
          type="number"
          className="input"
          placeholder="Codigo"
          required
        />
        <label className="titulos">Gramaje</label>
        <input
          value={gramaje}
          onChange={(e) => setGranaje(e.target.value)}
          type="ckeckbox"
          className="input"
          placeholder="Gramaje"
          required
        />

        <label className="titulos">Tipo Animal</label>
        <input
          value={tipo_animal}
          onChange={(e) => setTipoAnimal(e.target.value)}
          type="text"
          className="input"
          placeholder="Tipo Animal"
          required
        />
        <label className="titulos">Descripcion</label>
        <input
          value={descripcion}
          onChange={(e) => setDescripcion(e.target.value)}
          type="text"
          className="input"
          placeholder="Descripcion"
          required
        />
        <button type="Submit" className="boton">
          Crear
        </button>

        <Link to={"/alimento"}>
          <button type="Submit" className="boton" style={{ float: "right" }}>
            Regresar
          </button>
        </Link>
      </form>
    </div>
  );
};

export default CompProductoAlmientoCrear;
