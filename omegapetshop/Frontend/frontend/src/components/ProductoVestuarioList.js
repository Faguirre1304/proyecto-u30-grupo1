import axios from "axios";

//HOOK --> GANCHOS --> Gestionar los estados del componente
import { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import "./Estilos.css";

const URI = "http://localhost:4200/";

const CompProductoVestuarioListar = () => {
  const [productoVestuario, setVestuario] = useState([]);
  useEffect(() => {
    getVestuario();
  }, []);

  //METODOS DEL COMPONENTE

  //METODO PARA LISTAR LAS RUTAS DE VUELO

  const getVestuario = async () => {
    const res = await axios.get(`${URI}listarvestuario/`);

    setVestuario(res.data.message);
  };

  //METODO PARA ELIMINAR LAS RUTAS DE VUELO

  const deleteProductoVestuario = async (id) => {
    await axios.delete(`${URI}eliminarvestuario/${id}`);
    getVestuario();
  };

  return (
    <div className="container-tabla">
      <img src="../images/LogoPaginaNT.png" alt="logo" />
      <h1 className="titulos">Lista Vestuario</h1>
      <div className="row" style={{ margin: "10px" }}>
        <div className="col">
          <Link
            to={"/crearvestuario"}
            className="crear"
            style={{ marginLeft: "70px" }}
          >
            <i className="fa-solid fa-file-circle-plus" title="Crear">
              {" "}
            </i>{" "}
            Crear Producto
          </Link>

          <Link to={"/"} className="crear">
            <i className="fa-solid fa-file-circle-plus" title="Home">
              {" "}
            </i>{" "}
            Menu Principal
          </Link>
          <table>
            <thead>
              <tr>
                <th>Nombre Producto</th>
                <th>Categoria Producto</th>
                <th>Precio</th>
                <th>Stock</th>
                <th>Codigo</th>
                <th>Tallas</th>
                <th>Tipo Animal</th>
                <th>Descripcion</th>
                <th>Acciones</th>
              </tr>
            </thead>

            <tbody>
              {productoVestuario.map((vestuario) => (
                <tr key={vestuario._id}>
                  <td>{vestuario.nombre_producto}</td>
                  <td>{vestuario.categoria_producto}</td>
                  <td>{vestuario.precio}</td>
                  <td>{vestuario.stock}</td>
                  <td>{vestuario.codigo}</td>
                  <td>{vestuario.tallas}</td>
                  <td>{vestuario.tipo_animal}</td>
                  <td>{vestuario.descripcion}</td>

                  <td>
                  <Link
                      to={`/editarvestuario/${vestuario._id}`}
                      className="fa-regular fa-pen-to-square"
                      style={{ color: "#000" }}
                    ></Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link
                      onClick={() => deleteProductoVestuario(vestuario._id)}
                      className="fa-solid fa-trash-can"
                      style={{ color: "#EE3724" }}
                    ></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default CompProductoVestuarioListar;
