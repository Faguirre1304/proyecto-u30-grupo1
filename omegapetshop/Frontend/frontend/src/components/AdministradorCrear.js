import axios from "axios";

import { useState } from "react";

import { useNavigate, Link } from "react-router-dom";

const URI = "http://localhost:4200/crearadmon/";

const CompAdminCrear = () => {
  //IMPLEMENTACION DE LOS HOOKS

  const [nombre, setNombre] = useState("");
  const [correo, setCorreo] = useState("");
  const [documento, setDocumento] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const create = async (e) => {
    e.preventDefault();
    await axios.post(URI, {
     nombre: nombre.toLocaleUpperCase(),
     correo: correo.toLocaleUpperCase(),
     documento: documento,
     password: password,
    });

    //SI HAY STATUS 200
    navigate("/admin");
  };

 

  return (
    <div className="container-alimento">
      <img src="../images/LogoPaginaNT.png" alt="logo" />
      <h1 className="titulos">Creación Usuario Administrador</h1>
      <form onSubmit={create}>
      <label className="titulos">Nombre</label>
        <input
          value={nombre}
          onChange={(e) => setNombre(e.target.value)}
          type="text"
          className="input"
          
          placeholder={'Nombre..'}
          
          

        />
        <label className="titulos">Correo</label>
        <input
          value={correo}
          onChange={(e) => setCorreo(e.target.value)}
          type="text"
          className="input"
          placeholder={'Correo..'}
          
          
        />
         <label className="titulos">Documento</label>
        <input
          value={documento}
          onChange={(e) => setDocumento(e.target.value)}
          type="number"
          className="input"
          placeholder={'Documento..'}
          
          />
         <label className="titulos">Password</label>
        <input
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="text"
          className="input"
          placeholder={'Password..'}
          
        />
         
        <button type="Submit" className="boton">
          Crear
        </button>

        <Link to={"/admin"}>
          <button type="Submit" className="boton" style={{ float: "right" }}>
            Regresar
          </button>
        </Link>


      </form>
    </div>
  );
};

export default CompAdminCrear;
