import axios from "axios";

import { useEffect, useState } from "react";

import { useNavigate, useParams, Link } from "react-router-dom";

const URI = "http://localhost:4200/";

const CompAdminEdit = () => {
  //IMPLEMENTACION DE LOS HOOKS

  const [nombre, setNombre] = useState("");
  const [correo, setCorreo] = useState("");
  const [documento, setDocumento] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();
  const { id } = useParams();

  //METODO EDITAR

  const editaradmin = async (e) => {
    e.preventDefault();
    await axios.put(`${URI}actualizaradmon/${id}`, {
      nombre: nombre,
      correo: correo,
      documento: documento,
      password: password,
    });

    //SI HAY STATUS 200
    navigate("/admin");
  };

  useEffect(() => {
    getAlimentosById();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getAlimentosById = async () => {
    const res = await axios.get(`${URI}buscaradmon/${id}`);
    setNombre(res.data.message.nombre);
    setCorreo(res.data.message.correo);
    setDocumento(res.data.message.documento);
    setPassword(res.data.message.password);
  };

  return (
    <div className="container-alimento">
       <img src="../images/LogoPaginaNT.png" alt="logo" />
      <h1>Editar Usuario</h1>
      <form onSubmit={editaradmin}>
        <label className="titulos">Nombre</label>
        <input
          value={nombre}
          onChange={(e) => setNombre(e.target.value)}
          type="text"
          className="input"
        />
        <label className="titulos">Correo</label>
        <input
          value={correo}
          onChange={(e) => setCorreo(e.target.value)}
          type="text"
          className="input"
        />
        <label className="titulos">Documento</label>
        <input
          value={documento}
          onChange={(e) => setDocumento(e.target.value)}
          type="text"
          className="input"
        />
        <label type="password" className="titulos">
          Password
        </label>
        <input
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="text"
          className="input"
        />

        <button type="Submit" className="boton">
          Editar
        </button>

        <Link to={"/admin"}>
          <button type="Submit" className="crear" style={{ float: "right" }}>
            Regresar
          </button>
        </Link>




      </form>
    </div>
  );
};

export default CompAdminEdit;
